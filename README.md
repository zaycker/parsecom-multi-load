Parse.com tool for uploading multiple records to different classes of datastorage.

This code was made as a hook for change event of table which plays a role of data source.
It gives an ability to skip implementation of any custom forms and etc.
It processes parameters from table which stores classes and it's new data every time it changed.

For using this code you have to:

1.   Create new "class" with name form constant UPLOADING_TABLE of this file. It would be a datasource table
     which will store CSV files and it's respective classes.
     By default it is 'UploadSource'.

2.   This class has to have two columns which names are stored in constants CLASS_NAMES_COLUMN and FILES_COLUMN.
     By default they are "class" with type of "String" and "file" with type of "File" respectively.

Each time you change data inside this table and fields CLASS_NAMES_COLUMN and FILES_COLUMN are not empty
in the changed row hook is started.
