/**
 * This code made as a hook for change event of table which plays a role of data source.
 * It gives an ability to skip implementation of any custom forms and etc.
 * It processes parameters from table which stores classes and it's new data every time it changed.
 *
 * For using this code you have to:
 *
 * 1.   Create new "class" with name form constant UPLOADING_TABLE of this file. It would be a datasource table
 *      which will store CSV files and it's respective classes.
 *      By default it is 'UploadSource'.
 *
 * 2.   This class has to have two columns which names are stored in constants CLASS_NAMES_COLUMN and FILES_COLUMN.
 *      By default they are "class" with type of "String" and "file" with type of "File" respectively.
 *
 * Each time you change data inside this table and fields CLASS_NAMES_COLUMN and FILES_COLUMN are not empty
 * in the changed row hook is started.
 *
 * Bonus: If a class from CLASS_NAMES_COLUMN is not exist it will be created.
 *
 * Only rows marked "new" in the last column would be added.
 */

/**
 * @type {string}
 */
var UPLOADING_TABLE = 'UploadSource';

/**
 * @type {string}
 */
var CLASS_NAMES_COLUMN = 'class';

/**
 * @type {string}
 */
var FILES_COLUMN = 'file';

/**
 * @type {string}
 */
var RECORDS_DELIMITER_IN_ROW = 'new';

/**
 * Defines hook
 */
Parse.Cloud.afterSave(UPLOADING_TABLE, function (request) {
    var className = request.object.get(CLASS_NAMES_COLUMN),
        file = request.object.get(FILES_COLUMN);

    if (!(className && file && file.url())) {
        return;
    }

    readCSVFile(file.url(), function (csvData) {
        addObjectsToClass(className, csvData);
    });
});

/**
 * Defines Parse.com function (not used by default)
 */
Parse.Cloud.define('ImportCustomCSV', readCSVFile);

/**
 * @param {string} className
 * @param {Array.<Array.<string>>} rows
 */
function addObjectsToClass(className, rows) {
    var RecordClass = Parse.Object.extend(className);

    rows.map(processRow).reduce(function (recordsArray, rowArray) {
        return recordsArray.concat(rowArray);
    }, []).forEach(function (obj) {
        var record = new RecordClass();
        record.save(obj);
    });
}
