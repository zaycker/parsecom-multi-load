/**
 * https://gist.githubusercontent.com/bennadel/9753411/raw/a8e6f25f15fc78d1ef2d187e4f4864c4b528f885/code-1.htm
 * @param {string} strData
 * @param {string} strDelimiter
 * @return {[]}
 */
function CSVToArray(strData, strDelimiter) {
    strDelimiter = (strDelimiter || ",");
    var objPattern = new RegExp(
        (
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
    );
    var arrData = [[]];
    var arrMatches;
    while (arrMatches = objPattern.exec(strData)) {
        var strMatchedDelimiter = arrMatches[1];
        if (
            strMatchedDelimiter.length &&
            (strMatchedDelimiter != strDelimiter)
        ) {
            arrData.push([]);
        }
        if (arrMatches[2]) {
            var strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"),
                "\""
            );
        } else {
            var strMatchedValue = arrMatches[3];
        }
        arrData[arrData.length - 1].push(strMatchedValue);
    }
    return ( arrData );
}

/**
 * @param {Array.<string>} row
 * @returns {Array.<object>}
 */
function processRow (row) {
    return recursiveProcessRow(row, []);
}

/**
 * @param {Array.<string>} row
 * @param {Array.<object>} previous
 * @returns {Array.<object>}
 */
function recursiveProcessRow(row, previous) {
    previous = previous || [];

    var recordObject = {},
        key, isNotEmpty = false;
    while (row.length) {
        key = row.shift().trim();

        if ([RECORDS_DELIMITER_IN_ROW, ''].indexOf(key) !== -1) {
            return recursiveProcessRow(row, isNotEmpty ? previous.concat([recordObject]) : previous);
        }

        if (!row.length) {
            continue;
        }

        recordObject[key] = row.shift().trim();
        isNotEmpty = true;
    }
    return isNotEmpty ? previous.concat([recordObject]) : previous;
}

/**
 * @param {string} url
 * @param {function} callback
 */
function readCSVFile(url, callback) {
    Parse.Cloud.httpRequest({url: url}).then(function (response) {
        callback(CSVToArray(response.buffer.toString()));
    });
}

exports = {
    paeaw
}
